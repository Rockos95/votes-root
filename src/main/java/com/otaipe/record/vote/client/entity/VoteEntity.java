package com.otaipe.record.vote.client.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

/**
 * Vote entity.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
@Entity(name = "vote")
public class VoteEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -374184125987342332L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Integer id;

	private String description;

	@Column(nullable = false)
	private Integer idUserVoting;

	@Column(nullable = false)
	private Integer idUserVote;

	@Column(nullable = false)
	private Integer idArea;

	@Column(nullable = false)
	private Boolean status;

	@Column(nullable = false)
	private Date dateCreated;

	private Date dateModified;

	@JoinColumn(name = "idUserVoting", referencedColumnName = "id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private UserEntity userVoting;

	@JoinColumn(name = "idUserVote", referencedColumnName = "id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private UserEntity userVote;

	@JoinColumn(name = "idArea", referencedColumnName = "id", insertable = false, updatable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private AreaEntity area;

}