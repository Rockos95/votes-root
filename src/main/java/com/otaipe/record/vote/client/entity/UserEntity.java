package com.otaipe.record.vote.client.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * User entity.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
@Entity(name = "user")
@EqualsAndHashCode
public class UserEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5877063973477621482L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Integer id;
	
	@Column(nullable = false)
	private String firstName;
	
	@Column(nullable = false)
	private String lastName;

	@Column(nullable = false)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false)
	private Boolean status;

	@Column(nullable = false)
	private Date dateCreated;

	private Date dateModified;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<UserRolesEntity> userRoles;
}