package com.otaipe.record.vote.client.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

/**
 * User roles entity.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
@Entity(name = "user_roles")
@EqualsAndHashCode
public class UserRolesEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8848134350647493101L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Integer id;

	@Column(nullable = false)
	private Integer idRol;

	@Column(nullable = false)
	private Integer idUser;

	@Column(nullable = false)
	private Boolean status;

	@Column(nullable = false)
	private Date dateCreated;

	private Date dateModified;
	
	@JoinColumn(name = "idUser", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private UserEntity user;
	
	@JoinColumn(name = "idRol", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private RolesEntity role;
}
