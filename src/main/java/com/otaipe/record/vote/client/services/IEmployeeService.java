package com.otaipe.record.vote.client.services;

/**
 * Employee service interface.
 * 
 * @author oswal
 *
 */
public interface IEmployeeService {

	/**
	 * Find total employees.
	 * 
	 * @return
	 */
	Long findTotalEmployess();
}
