package com.otaipe.record.vote.client.data;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * User data.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class UserData implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8658704969359168445L;

	private Integer idUser;
	private String username;
	private String password;
	private String roles;
}
