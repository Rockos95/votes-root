package com.otaipe.record.vote.client.entity;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "roles")
@EqualsAndHashCode
public class RolesEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8093052173117754197L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Integer id;

	@Column(nullable = false)
	private String nombre;

	@Column(nullable = false)
	private Boolean status;

	@Column(nullable = false)
	private Date dateCreated;

	private Date dateModified;
}