package com.otaipe.record.vote.client.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

/**
 * Area entity.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
@Entity(name = "area")
public class AreaEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5223603186118190850L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(nullable = false)
	private Integer id;

	@Column(nullable = false)
	private String nameArea;

	@Column(nullable = false)
	private Boolean status;

	@Column(nullable = false)
	private Date dateCreated;

	private Date dateModified;

}
