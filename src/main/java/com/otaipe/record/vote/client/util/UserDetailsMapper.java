package com.otaipe.record.vote.client.util;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * User details mapper.
 * 
 * @author oswal
 *
 */
public final class UserDetailsMapper {
	public static UserDetails build(String username, String password, List<String> roles) {
		return new User(username, password, getAuthorities(roles));
	}

	private static Set<? extends GrantedAuthority> getAuthorities(List<String> roles) {
		Set<SimpleGrantedAuthority> authorities = new HashSet<>();
		roles.forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" + role)));
		return authorities;
	}
}