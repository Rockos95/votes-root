package com.otaipe.record.vote.client.services;

import java.util.List;

import com.otaipe.record.vote.vo.request.VoteRequest;
import com.otaipe.record.vote.vo.response.TopEmployee;

/**
 * Vote service interface.
 * 
 * @author oswal
 *
 */
public interface IVoteService {

	/**
	 * Save vote
	 * 
	 * @param request represents a reques of vote
	 * @return
	 * @throws Exception
	 */
	void saveVote(VoteRequest request) throws Exception;

	/**
	 * Find top employees.
	 * 
	 * @param month represents a month
	 * @return
	 */
	List<TopEmployee> findTopEmployees(Integer month);

	/**
	 * Find max top employees.
	 * 
	 * @return
	 */
	List<Object> findMaxTopEmployees();
}
