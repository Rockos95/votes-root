package com.otaipe.record.vote.client.util;

import java.util.Calendar;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Date util.
 * 
 * @author oswal
 *
 */
public final class DateUtil {

	public static final Integer CURRENT_YEAR = DateTime.now().getYear();

	public static final Integer CURRENT_MONTH = DateTime.now().getMonthOfYear();

	public static final Integer LAST_DAY_OF_CURRENT_MONTH = DateTime.now().dayOfMonth().getMaximumValue();

	public static final Integer LAST_HOUR_OF_CURRENT_DAY = DateTime.now().hourOfDay().getMaximumValue();

	public static final Integer LAST_MINUTE_OF_CURRENT_HOUR = DateTime.now().minuteOfHour().getMaximumValue();

	public static final Integer LAST_SECOND_OF_CURRENT_MINUTE = DateTime.now().secondOfMinute().getMaximumValue();

	public static DateTime getLastDateOfMonth(Integer month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(CURRENT_YEAR, month-1, 1);
		TimeZone tz = calendar.getTimeZone();
		DateTimeZone jodaTz = DateTimeZone.forID(tz.getID());
		DateTime dateTime = new DateTime(calendar.getTimeInMillis(), jodaTz);
		return new DateTime(CURRENT_YEAR, month, dateTime.dayOfMonth().getMaximumValue(), dateTime.hourOfDay().getMaximumValue(),
				dateTime.minuteOfHour().getMaximumValue(), dateTime.secondOfMinute().getMaximumValue());
	}

	public static DateTime getLastDateOfMonth() {
		return new DateTime(CURRENT_YEAR, CURRENT_MONTH, LAST_DAY_OF_CURRENT_MONTH, LAST_HOUR_OF_CURRENT_DAY,
				LAST_MINUTE_OF_CURRENT_HOUR, LAST_SECOND_OF_CURRENT_MINUTE);
	}
}