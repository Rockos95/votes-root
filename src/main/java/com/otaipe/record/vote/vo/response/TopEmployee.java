package com.otaipe.record.vote.vo.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TopEmployee {
	private String firstName;
	private String lastName;
	private String area;
	private Long votos;
	
	public TopEmployee(String firstName,String lastName,Long votos) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.votos = votos;
	}
	
}
