package com.otaipe.record.vote.vo.request;

import lombok.Getter;
import lombok.Setter;

/**
 * Vote request;
 * 
 * @author oswal
 *
 */
@Getter
@Setter
public class VoteRequest {

	private Integer idArea;
	private Integer idUserVoting;
	private Integer idUserVote;
	private String description;

}
