package com.otaipe.record.vote.vo.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * User request.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
public class UserRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3549663359820976084L;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
}
