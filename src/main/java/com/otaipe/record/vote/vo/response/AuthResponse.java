package com.otaipe.record.vote.vo.response;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Auth response.
 * @author oswal
 *
 */
@Getter
@Setter
@AllArgsConstructor
public class AuthResponse implements Serializable {
	
	private static final long serialVersionUID = 6581974775291565107L;
	private String jwttoken;

}
