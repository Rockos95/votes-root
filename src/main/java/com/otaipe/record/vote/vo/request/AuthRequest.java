package com.otaipe.record.vote.vo.request;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * Auth request.
 * 
 * @author oswal
 *
 */
@Getter
@Setter
public class AuthRequest implements Serializable {

	private static final long serialVersionUID = -3768476728081134253L;
	private String username;
	private String password;

}