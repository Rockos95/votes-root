package com.otaipe.record.vote.service.config;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

/**
 * Json web token configuration.
 * 
 * @author oswal
 *
 */
@Component
@Slf4j
public class JwtConfig implements Serializable {
	private static final long serialVersionUID = -2550185165626007488L;
	public static final long JWT_TOKEN_EXP_TIME = 5 * 60 * 60;
	@Value("${jwt.secret}")
	private String secret;

	/**
	 * Get username from token.
	 * 
	 * @param token represents a token
	 * @return
	 */
	public String getUsernameFromToken(String token) {
		return getClaimFromToken(token, Claims::getSubject);
	}

	/**
	 * Get expiration date from token.
	 * 
	 * @param token represents a token
	 * @return
	 */
	public Date getExpirationDateFromToken(String token) {
		return getClaimFromToken(token, Claims::getExpiration);
	}

	/**
	 * Get claim from token
	 * 
	 * @param <T>
	 * @param token          represents a token
	 * @param claimsResolver represents the claims resolver
	 * @return
	 */
	public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
		final Claims claims = getAllClaimsFromToken(token);
		return claimsResolver.apply(claims);
	}

	/**
	 * Get all claims from token
	 * 
	 * @param token represents a token
	 * @return
	 */
	private Claims getAllClaimsFromToken(String token) {
		return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
	}

	/**
	 * Get if the token is expired.
	 * 
	 * @param token represents a token
	 * @return
	 */
	private Boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	/**
	 * Generate token
	 * 
	 * @param userDetails represents a user details
	 * @return
	 */
	public String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		return doGenerateToken(claims, userDetails.getUsername());
	}

	/**
	 * Do generate token
	 * 
	 * @param claims  represents a map of claims
	 * @param subject represents the user-name.
	 * @return
	 */
	private String doGenerateToken(Map<String, Object> claims, String username) {
		Date expirationDate = new Date(System.currentTimeMillis() + JWT_TOKEN_EXP_TIME * 10);
		log.info("expiration date" + expirationDate);
		return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	/**
	 * Validate token.
	 * 
	 * @param token       represents a token
	 * @param userDetails represents a user details
	 * @return
	 */
	public Boolean validateToken(String token, UserDetails userDetails) {
		final String username = getUsernameFromToken(token);
		return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
	}
}
