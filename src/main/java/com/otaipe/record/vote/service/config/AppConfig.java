package com.otaipe.record.vote.service.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.otaipe.record.vote.core.repository.UserRepository;

/**
 * Application configuration.
 * 
 * @author oswal
 *
 */
@EnableTransactionManagement
@EnableJpaRepositories(basePackageClasses = UserRepository.class)
@EntityScan(basePackages = { "com.otaipe.record.vote.client.entity" })
@ComponentScan(basePackages = { "com.otaipe.record.vote.core.services", "com.otaipe.record.vote.core.repository" })
public class AppConfig {

}
