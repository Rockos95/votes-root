package com.otaipe.record.vote.service.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.otaipe.record.vote.core.services.VoteService;
import com.otaipe.record.vote.vo.request.VoteRequest;
import com.otaipe.record.vote.vo.response.TopEmployee;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/vote")
@Lazy
@Slf4j
public class VotesController {

	@Autowired
	@Lazy
	private VoteService voteService;

	@RequestMapping({ "/hello" })
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String helloWorld() {
		return "Hello World";
	}

	/**
	 * Save vote.
	 * 
	 * @param request represents a vote request
	 * @return
	 */
	@PostMapping("/saveVote")
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<?> saveVote(@RequestBody VoteRequest request) {
		log.info("Init saveVote::VotesController");
		try {
			this.voteService.saveVote(request);
			return ResponseEntity.ok(HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}

	}

	@GetMapping("/findTopEmployee")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<TopEmployee> findTopEmployee(@RequestParam Integer month) {
		log.info("Init findTopEmployee::VotesController");
		return this.voteService.findTopEmployees(month);
	}
	
	
	@GetMapping("/findMaxTopEmployeesByArea")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public List<Object> findMaxTopEmployeesByArea() {
		log.info("Init findMaxTopEmployeesByArea::VotesController");
		return this.voteService.findMaxTopEmployees();
	}

}
