package com.otaipe.record.vote.service.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.otaipe.record.vote.core.services.EmployeeService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/employee")
@Lazy
@Slf4j
public class EmployeeController {

	@Autowired
	@Lazy
	private EmployeeService employeeService;
	
	

	@GetMapping("/getCountEmployee")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Long getCountEmployee() {
		log.info("Init getCountEmployee::EmployeeController");
		return this.employeeService.findTotalEmployess();
	}
	
	
	
	
	
}