package com.otaipe.record.vote.core.services;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.otaipe.record.vote.client.data.UserData;
import com.otaipe.record.vote.client.entity.UserEntity;
import com.otaipe.record.vote.client.util.UserDetailsMapper;
import com.otaipe.record.vote.core.repository.UserRepository;
import com.otaipe.record.vote.vo.request.UserRequest;

/**
 * Authentication service.
 * 
 * @author oswal
 *
 */
@Validated
@Service
public class AuthService implements UserDetailsService {

	@Autowired
	@Lazy
	private UserRepository userRepository;

	@Autowired
	@Lazy
	private PasswordEncoder bcryptEncoder;

	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		if (userName != null && userName != "") {
			final List<UserData> user = this.findUserByUsername(userName);
			if (CollectionUtils.isNotEmpty(user)) {
				UserData userData = user.iterator().next();
				return UserDetailsMapper.build(userData.getUsername(), userData.getPassword(),
						user.stream().map(UserData::getRoles).collect(Collectors.toList()));
			} else {
				throw new UsernameNotFoundException("User not found with username: " + userName);
			}

		} else {
			throw new UsernameNotFoundException("User not found with username: " + userName);
		}
	}

	/**
	 * Find user by user-name.
	 * 
	 * @param username represents a user-name
	 * @return
	 */
	@Transactional(readOnly = true)
	public List<UserData> findUserByUsername(String username) {
		return this.userRepository.findUserByUsername(username);
	}

	/**
	 * Save user.
	 * 
	 * @param user represents a user request.
	 * @return
	 */
	public UserEntity save(UserRequest user) {
		UserEntity newUser = new UserEntity();
		newUser.setUsername(user.getUsername());
		newUser.setFirstName(user.getFirstName());
		newUser.setLastName(user.getLastName());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setStatus(Boolean.TRUE);
		newUser.setDateCreated(new Date());
		return userRepository.save(newUser);
	}

}
