package com.otaipe.record.vote.core.services;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.otaipe.record.vote.client.entity.VoteEntity;
import com.otaipe.record.vote.client.services.IVoteService;
import com.otaipe.record.vote.client.util.DateUtil;
import com.otaipe.record.vote.core.repository.VoteRepository;
import com.otaipe.record.vote.vo.request.VoteRequest;
import com.otaipe.record.vote.vo.response.TopEmployee;

/**
 * Vote services.
 * 
 * @author oswal
 *
 */
@Validated
@Service
public class VoteService implements IVoteService {

	private static final Integer FIRST_DAY_OF_MONTH = 1;

	@Autowired
	@Lazy
	private VoteRepository voteRepository;

	@Override
	public void saveVote(VoteRequest request) throws Exception {
		if (!request.getIdUserVote().equals(request.getIdUserVoting())) {
			LocalDate localDate = LocalDate.now();
			LocalDate start = localDate.withDayOfMonth(FIRST_DAY_OF_MONTH);
			ZoneId defaultZoneId = ZoneId.systemDefault();
			List<VoteEntity> existVote = this.voteRepository.findVoteByVote(request.getIdUserVote(),
					request.getIdUserVoting(), request.getIdArea(),
					Date.from(start.atStartOfDay(defaultZoneId).toInstant()), DateUtil.getLastDateOfMonth().toDate());
			if (!CollectionUtils.isNotEmpty(existVote)) {
				VoteEntity vote = new VoteEntity();
				vote.setIdArea(request.getIdArea());
				vote.setIdUserVote(request.getIdUserVote());
				vote.setIdUserVoting(request.getIdUserVoting());
				vote.setDateCreated(new Date());
				vote.setDescription(request.getDescription());
				vote.setStatus(Boolean.TRUE);
				this.voteRepository.save(vote);

			} else {
				throw new Exception("Ya existe una votacion por la persona seleccionada");
			}
		} else {
			throw new Exception("No se puede realizar una votacion entre s� mismo");
		}
	}

	@Override
	public List<TopEmployee> findTopEmployees(Integer month) {
		LocalDate localDate = LocalDate.now();
		localDate = localDate.withMonth(month);
		LocalDate start = localDate.withDayOfMonth(FIRST_DAY_OF_MONTH);
		ZoneId defaultZoneId = ZoneId.systemDefault();
		return this.voteRepository.findTopEmployee(Date.from(start.atStartOfDay(defaultZoneId).toInstant()),
				DateUtil.getLastDateOfMonth(month).toDate());

	}

	@Override
	public List<Object> findMaxTopEmployees() {
		return this.voteRepository.findMaxTopEmployeesByArea();
	}
}
