package com.otaipe.record.vote.core.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import com.otaipe.record.vote.client.services.IEmployeeService;
import com.otaipe.record.vote.core.repository.UserRepository;

/**
 * Employee service.
 * 
 * @author oswal
 *
 */
@Validated
@Service
public class EmployeeService implements IEmployeeService {

	@Autowired
	@Lazy
	private UserRepository userRepository;

	@Override
	public Long findTotalEmployess() {
		return this.userRepository.getCountEmployee();
	}

}
