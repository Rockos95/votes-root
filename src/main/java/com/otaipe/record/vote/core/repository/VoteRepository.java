package com.otaipe.record.vote.core.repository;

import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.otaipe.record.vote.client.entity.VoteEntity;
import com.otaipe.record.vote.vo.response.TopEmployee;

/**
 * Vote repository.
 * 
 * @author oswal
 *
 */
@Repository
@Lazy
public interface VoteRepository extends JpaRepository<VoteEntity, Integer> {

	@Query(value = "select v from vote v  where v.idUserVoting = :idUserVoting and v.idUserVote = :idUserVote and v.status=1 and v.idArea = :idArea and v.dateCreated between :beginDate  AND :endDate")
	List<VoteEntity> findVoteByVote(@Param("idUserVote") Integer idUserVote,
			@Param("idUserVoting") Integer idUserVoting, @Param("idArea") Integer idArea,
			@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);

	@Query(value = "select new com.otaipe.record.vote.vo.response.TopEmployee(uv.firstName, uv.lastName, COUNT(v.idUserVoting)) from vote v INNER JOIN v.userVoting uv where v.dateCreated between :beginDate AND :endDate GROUP BY v.idUserVoting order by COUNT(v.idUserVoting) desc")
	List<TopEmployee> findTopEmployee(@Param("beginDate") Date beginDate, @Param("endDate") Date endDate);
	
	@Query(value = "SELECT firstname,lastname, area, max(total) as votos FROM (SELECT u.first_name as firstname, u.last_name as lastname,a.name_area as area, COUNT(v.id_user_voting) as total FROM vote v inner join user u on v.id_user_voting = u.id inner join area a on v.id_area=a.id group by v.id_user_voting ) as result group by area", nativeQuery = true)
	List<Object> findMaxTopEmployeesByArea();

}

