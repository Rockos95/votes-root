package com.otaipe.record.vote.core.repository;

import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.otaipe.record.vote.client.data.UserData;
import com.otaipe.record.vote.client.entity.UserEntity;

/**
 * User repository.
 * 
 * @author oswal
 *
 */
@Repository
@Lazy
public interface UserRepository extends JpaRepository<UserEntity, Integer> {

	/**
	 * Find user by user-name.
	 * 
	 * @param userName represents a user-name
	 * @return
	 */
	@Query(value = "select new com.otaipe.record.vote.client.data.UserData(u.id,u.username, u.password, r.nombre) "
			+ "from user u INNER JOIN u.userRoles ur INNER JOIN ur.role r where u.username = :username")
	List<UserData> findUserByUsername(@Param("username") String username);
	
	@Query(value  ="select COUNT(u) from user u where u.status = 1")
	Long getCountEmployee();
	
	

}
