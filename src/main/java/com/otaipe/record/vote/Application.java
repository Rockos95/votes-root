package com.otaipe.record.vote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.otaipe.record.vote.service.config.AppConfig;

@SpringBootApplication
@Import({ AppConfig.class,})
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
