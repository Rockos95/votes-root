package com.otaipe.record.vote.repository;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.otaipe.record.vote.SpringBootTest;
import com.otaipe.record.vote.core.repository.UserRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { SpringBootTest.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class UserRepositoryTest {

	private static final String USER_NAME = "root";

	@Resource
	private UserRepository repository;

	@Test
	public void testFindByUserName() {
		Assert.assertTrue(!repository.findUserByUsername(USER_NAME).isEmpty());
	}
	
	@Test
	public void testGetEmployee() {
		Assert.assertTrue(repository.getCountEmployee()!=null);
	}

}
