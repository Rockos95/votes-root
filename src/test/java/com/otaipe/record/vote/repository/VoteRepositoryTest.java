package com.otaipe.record.vote.repository;

import java.util.Date;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.otaipe.record.vote.SpringBootTest;
import com.otaipe.record.vote.core.repository.VoteRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = { SpringBootTest.class })
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class VoteRepositoryTest {

	private static final Integer AREA = 1;
	private static final Integer USER_VOTE_ID = 1;
	private static final Integer USER_VOTING_ID = 1;

	@Resource
	private VoteRepository repository;

	@Test
	public void testFindTopEmployee() {
		Assert.assertTrue(this.repository.findTopEmployee(new Date(), new Date()).isEmpty());
	}

	@Test
	public void testFindVoteByVote() {
		Assert.assertTrue(
				this.repository.findVoteByVote(USER_VOTE_ID, USER_VOTING_ID, AREA, new Date(), new Date()).isEmpty());
	}

}
