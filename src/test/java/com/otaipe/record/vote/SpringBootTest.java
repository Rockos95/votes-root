package com.otaipe.record.vote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.otaipe.record.vote.service.config.AppConfig;

/**
 * Test spring boot.
 * 
 * @author otaipe
 */
@SpringBootApplication
@Import({ AppConfig.class })
public class SpringBootTest extends SpringBootServletInitializer implements WebMvcConfigurer {

	/**
	 * Main run spring boot app.
	 *
	 * @param args an array of {@link String} objects.
	 */
	public static void main(String[] args) {
		SpringApplication.run(SpringBootTest.class, args);

	}

}
