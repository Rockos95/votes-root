package com.otaipe.record.vote.service;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.otaipe.record.vote.core.repository.VoteRepository;
import com.otaipe.record.vote.core.services.VoteService;

/**
 * Vote service test.
 * 
 * @author oswal
 *
 */
@RunWith(MockitoJUnitRunner.Silent.class)
public class VoteServiceTest {

	private static final Integer AREA = 1;
	private static final Integer USER_VOTE_ID = 1;
	private static final Integer USER_VOTING_ID = 1;
	private static final Integer MONTH = 7;

	@InjectMocks
	private VoteService service;

	@Mock
	private VoteRepository voteRepository;

	@Before
	public void setup() {
		when(voteRepository.findVoteByVote(USER_VOTE_ID, USER_VOTING_ID, AREA, new Date(), new Date()))
				.thenReturn(new ArrayList<>());
		when(voteRepository.findMaxTopEmployeesByArea()).thenReturn(new ArrayList<>());
	}

	@Test
	public void testFindMaxTopEmployees() {
		Assert.assertTrue(this.service.findMaxTopEmployees().isEmpty());
	}

	@Test
	public void test() {
		this.service.findTopEmployees(MONTH);
	}

}