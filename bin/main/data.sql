INSERT INTO votesdb.area (id,date_created,date_modified,name_area,status) VALUES 
(1,'2020-07-07 00:00:00.0',NULL,'Team player',1)
,(2,'2020-07-07 00:00:00.0',NULL,'Technical referent',1)
,(3,'2020-07-07 00:00:00.0',NULL,'Key Player',1)
,(5,'2020-07-07 00:00:00.0',NULL,'Client satisfaction',1);


INSERT INTO votesdb.roles (id,date_created,date_modified,nombre,status) VALUES 
(1,'2020-07-07 00:00:00.0',NULL,'ADMIN',1)
,(2,'2020-07-07 00:00:00.0',NULL,'USER',1)
;

INSERT INTO votesdb.`user` (id,date_created,date_modified,password,status,username,first_name,last_name) VALUES 
(1,'2020-07-08 00:00:00.0',NULL,'$2a$10$slYQmyNdGzTn7ZLBXBChFOC9f6kFjAqPhccnP6DxlWXx2lPk1C3G6',1,'root','Andres','Cueva')
,(4,'2020-07-08 06:48:44.0',NULL,'$2a$10$GUS.BoIiNHzWJHr2/7HAjONfJzDW8AVK/inWJIoX8SM1SBM5ZTgEy',1,'andres','Marlon','Calero')
,(5,'2020-07-08 06:48:49.0',NULL,'$2a$10$lPyAzAOFsze14FJso2mdzO.0NoIXVcA8uZzibTOfp/vdk0jpKJfaW',1,'luis','Oswaldo','Taipe')
,(6,'2020-07-08 06:48:53.0',NULL,'$2a$10$Awne4j1nPVuJm9ah1C1D.ugGys2MCM98h99oTYz71ymc0ubQm5ZDi',1,'carlos','Diana','Rios')
,(7,'2020-07-08 06:48:57.0',NULL,'$2a$10$gL3XyBwHvtmu9ra/WKjM..4CuFX5XF92BGfjsghBBc7KO7bPS7LyG',1,'diana','Andrea','Merlo')
,(8,'2020-07-08 06:49:01.0',NULL,'$2a$10$qiI0nqaNXx0w4TaOAl8YpuqfPJVE.eAUQ9u22trSr7JykLcLf0tPS',1,'carolina','Darwin','Herrera')
,(9,'2020-07-08 06:49:05.0',NULL,'$2a$10$bRN0lwbfWjzyInF2fbuvJet8D81C6eeni4WN3PX5dll3JrdPN3k8m',1,'darwin','Luis','Beltran')
,(10,'2020-07-08 06:49:23.0',NULL,'$2a$10$MC7GAn9mBjC1U1DhZooT4.5qsSEfPNxxwbDJ80nPSOCs49Ix6q8p2',1,'kevin','Iveth','Guzman')
,(23,'2020-07-08 22:18:05.0',NULL,'$2a$10$59KLwWJswZzfHJ4/iF4YuuGlgLAHXoyxdgaQhl76csCkaH7aGHMy6',1,'user','prueba','prueba')
;

INSERT INTO votesdb.user_roles (id,date_created,date_modified,id_rol,id_user,status) VALUES 
(1,'2020-07-07 00:00:00.0',NULL,1,1,1)
,(2,'2020-07-07 00:00:00.0',NULL,2,1,1)
,(3,'2020-07-07 00:00:00.0',NULL,2,8,1)
;


INSERT INTO votesdb.vote (id,date_created,date_modified,description,id_area,id_user_vote,id_user_voting,status) VALUES 
(14,'2020-07-08 16:50:16.0',NULL,NULL,1,1,5,1)
,(15,'2020-07-08 16:50:40.0',NULL,NULL,2,1,8,1)
,(16,'2020-07-08 16:50:45.0',NULL,NULL,2,1,9,1)
,(17,'2020-07-08 16:50:50.0',NULL,NULL,3,1,10,1)
,(18,'2020-07-08 16:50:58.0',NULL,NULL,2,5,10,1)
,(19,'2020-07-08 16:51:02.0',NULL,NULL,1,5,8,1)
,(20,'2020-07-08 16:51:07.0',NULL,NULL,1,5,9,1)
,(24,'2020-07-08 22:23:25.0',NULL,NULL,1,5,10,1)
;