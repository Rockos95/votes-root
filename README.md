# README #
	VOTES-ROOT
	Version 1.0.0-SNAPSHOT
	
### Estructura ###  
El proyecto esta realizado con springboot en su version 2.3.1-RELEASE; Se utiliza una estructura por capas    
	1. VO => Paquete donde se agregaran vista para respuestas o peticiones de objetos  
	2. Client => Paquete donde se encuentran los mapeos de entidades, interfaces y clases utilitarias  
	3. Core => Paquete donde se encuentra la logica del negocio  
	4. Services => Paquete donde estar la configuracion de springboot  y los controladores para los ws  		


### Configuraci�n ###

Base de datos
La base de datos ser� mysql 5.7 en el puerto 3308  
Se debera crear una nueva base de datos con el nombre "votesdb"  

Proyecto:  
	1. Clonar el repositorio  
	2. Ir a la raiz de la carpeta  
	3. Ejecutar el comando gradlew clean build -x test  
	4. Ejecutar gradlew clean bootRun  
El servicio estara levantado en el puerto 8080  	
	
	
Credenciales
Todos los usuarios generados como prueba dentro de la base de datos tiene su contrase�a como "password"


POSTMAN
Para comprbar los servicio web se agrego el json para importar los ws.  
https://bitbucket.org/Rockos95/votes-root/src/master/src/main/resources/VOTES-WS.postman_collection.json  

